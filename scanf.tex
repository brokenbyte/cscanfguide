\documentclass[a4,8pt]{article}

\usepackage{minted}
\usepackage{parskip}
\usepackage{multicol}
\usepackage{multirow}
\usepackage{graphicx}
\usepackage[margin=1in]{geometry}
\usepackage{tikz}
\usepackage{setspace}

\graphicspath{{./images/}}
\renewcommand{\newline}{\hfill\break}

\begin{document}
\setcounter{secnumdepth}{0}
\pagenumbering{gobble}
\begin{center}
  \section{A Short Guide to \mintinline{c}{scanf()} with C/C++}
  \mintinline{c}{scanf("format-string",arg1[,arg2,...,argN]);}
\end{center}

\mintinline{c}{scanf}, short for \emph{scan formatted}, is used for
reading input matching a specific pattern, or \emph{format},
from \mintinline{c}{stdin} and storing it in to variables.

A \mintinline{c}{scanf} statement consists of the format string, which contains format specifiers and
a comma separated list of variable \emph{addresses} to store the input in.

\subsection{Format Specifiers}
The syntax for using format specifiers is as follows (square brackets denote optional parameters):\\
\begin{center}
  \texttt{\%{\color{teal}[modifier]}conversion-character}
\end{center}

\subsection{Modifiers}
The \texttt{modifier} section of a format specifier is used to modify how the
input is converted and stored in the corresponding \texttt{arg}. The following
is a short list with some of available modifiers:

\begin{tabular}{r l}
  h  :& indicates the conversion will be one of \texttt{d, i, o, u, x, X, n}; corresponding \texttt{arg} must\\
      & be address of a \texttt{short int} or \texttt{unsigned short int} instead of \texttt{int}\\
  hh :& same as \texttt{h}, but corresponding \texttt{arg} must be \texttt{signed char} or \texttt{unsigned char}\\
  l  :& same as \texttt{h}, but corresponding \texttt{arg} must be \texttt{long int} or \texttt{unsigned long int}, or the\\
      & conversion will be one of \texttt{e, f, g}; corresponding \texttt{arg} must be address of \texttt{double} instead of \texttt{float}\\
\end{tabular}

\subsection{Conversion-Characters}
The \texttt{conversion-character} section is where you tell \texttt{scanf} what
data types you're reading in. The following is a short list with some of
the available conversion characters:

\begin{tabular}{r l}
  d  :&  matches an optionally signed decimal integer; corresponding \texttt{arg} must be address of an \texttt{int} \\
  i  :&  matches same as \texttt{d}, but the integer is read in base 16 if it begins with `\texttt{0x}' or `\texttt{0X}',\\
      &  base 8 if it begins with `\texttt{0}', and base 10 otherwise. Only characters that correspond to the base are used.\\
  o  :&  matches an unsigned octal integer; corresponding \texttt{arg} must be address of an \texttt{unsigned int}\\
  u  :&  matches an unsigned decimal integer; corresponding \texttt{arg} must be address of an \texttt{unsigned int}\\
  x  :&  matches an unsigned hexadecimal integer; corresponding \texttt{arg} must be address of an \texttt{unsigned int}\\
  X  :&  matches same as \texttt{x}\\
  f  :&  matches an optionally signed floating-point number; corresponding \texttt{arg} must be address of \texttt{float}\\
  e  :&  matches same as \texttt{f}\\
  g  :&  matches same as \texttt{f}\\
  E  :&  matches same as \texttt{f}\\
  s  :&  matches a sequence of non-whitepace characters; corresponding \texttt{arg} must be addres of a \texttt{char} array\\
      &  large enough for the input; input stops at whitespace\\
  n  :&  nothing is expected; instead the number of characters consumed thus far are stored through the next\\
      &  pointer, which  must be the address of an \texttt{int}\\
  \% :& matches a literal '\texttt{\%}', consumes leading whitespace; no assignment to a variable is made
\end{tabular}

\subsubsection{Examples}
\begin{center}
  {\small
    \begin{tabular}{|l|c|c|}
      \hline
      \multicolumn{1}{|c|}{Source code}                                & \multicolumn{1}{|c|}{Input} & \multicolumn{1}{|c|}{Output}                     \\
      \hline
      \mintinline{c}{scanf("%d",&id);}                                  & \multirow{2}{*}{52}         & \multirow{2}{*}{Your id is: 52}                  \\
      \mintinline{c}{printf("Your id is: %d\n",id);}                   &                             &                                                  \\
      \hline
      \mintinline{c}{scanf("%s",&name);}                                & \multirow{2}{*}{Meacham}    & \multirow{2}{*}{Your name is Meacham}            \\
      \mintinline{c}{printf("Your name is %s\n",name);}                &                             &                                                  \\
      \hline
      \mintinline{c}{scanf("%X",&hex);}                                 & \multirow{2}{*}{0xDEADBEEF} & \multirow{2}{*}{You entered 3735928559d}         \\
      \mintinline{c}{printf("You entered %ud\n",hex);}                 &                             &                                                  \\
      \hline
      \mintinline{c}{scanf("%ld",&longInt);}                            & \multirow{2}{*}{9991337999} & \multirow{2}{*}{Your long int is: 9991337999}    \\
      \mintinline{c}{printf("Your long int is: %ld\n",longInt);}       &                             &                                                  \\
      \hline
      \mintinline{c}{scanf("%f%%",&grade);}                             & \multirow{2}{*}{87.7\%}     & \multirow{2}{*}{Your grade is: 82.7}             \\
      \mintinline{c}{printf("Your grade is: %.2f\n",grade);}            &                             &                                                  \\
      \hline
      \mintinline{c}{scanf("%s%n",&class,&length);}                     & \multirow{2}{*}{CSC240}     & \multirow{2}{*}{Your class name is 6 letters}    \\
      \mintinline{c}{printf("Your class name is %d letters\n",length);} &                             &                                                  \\
      \hline
    \end{tabular}
  }
\end{center}

\subsection{How it knows where to place values}
When \texttt{scanf} scans through \texttt{stdin}, it replaces the format-specifiers with the arguments it is given, going from left to right.
For example, if prompted for input in the format of ``\texttt{name -- id -- salary}'', the assignment takes place as follows:\\
\begin{center}
  \hspace*{1.9cm}\begin{tikzpicture}
    \draw[blue,<->] (0,0) .. controls(1,1) and (2,1) .. (3.0,0);
    \draw[red,<->] (1,0) .. controls(1,1) and (4,1) .. (4.3,0);
    \draw[orange,<->] (2,0) .. controls(2,1) and (5,1) .. (5,0);
  \end{tikzpicture}\\
  \mintinline{c}{         scanf("%s -- %d -- %f", name, &id, &salary);}
\end{center}

\end{document}
